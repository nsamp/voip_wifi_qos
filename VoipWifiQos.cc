/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2019 AGH University of Science and Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as 
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Authors:  - Bartłomiej Godlewski 
 *           - Krzystof Jania
 *           - Nelsandro Sampaio
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/propagation-module.h"
#include "ns3/mobility-module.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/node-list.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv4-address.h"
//building//
#include <ns3/buildings-module.h>
#include <ns3/building.h>
#include <ns3/buildings-helper.h>
#include <ns3/buildings-propagation-loss-model.h>
#include <ns3/constant-position-mobility-model.h>
#include <ns3/hybrid-buildings-propagation-loss-model.h>
#include <ns3/constant-position-mobility-model.h>
#include <ns3/hybrid-buildings-propagation-loss-model.h>
#include <ns3/qos-blocked-destinations.h>
#include <iostream>
#include <vector>
#include <math.h>
#include <string>
#include <fstream>
#include <string>
#include <ctime>
#include <iomanip>


using namespace ns3;


NS_LOG_COMPONENT_DEFINE ("VoIP-WiFi-QoS");


class SimulationHelper 
{
public:
	SimulationHelper ();
	static void PopulateArpCache ();
	
};

SimulationHelper::SimulationHelper () 
{
}

// Functions Declarations
void mobilityAlloc(NodeContainer &nodes, double dist);
void mobilityAllocA(NodeContainer &nodes, double dist);
void installTrafficGenerator(Ptr<ns3::Node> fromNode, Ptr<ns3::Node> toNode, int port, std::string offeredLoad, int packetSize, int simulationTime, int warmupTime );
void createBuilding();

OnOffHelper trafficHelper (InetSocketAddress socketAddress, DataRate dataRate, int packetSize, uint8_t tid, Time start, Time stop);
OnOffHelper videoTraffic(InetSocketAddress socketAddress, DataRate rate, uint32_t pSize, Time appsStart, Time simTime);



int main (int argc, char *argv[])
{
  
	/* Variable declarations*/
  uint32_t nStations = 10;
	double distance = 1.0; //meters
  int APs = 1;
  bool oneDest = true;
  int simTime = 10;
  int packetSize = 1472;
	Time appsStart = Seconds (0);
 	float calcStart = 0;
 	double Mbps = 1000000;
 	uint32_t seed = 2;
  bool enableRtsCts = true;
  bool pcap = false;
  bool A_VO = true;
  bool VO = true;
  bool VI = true;
  bool A_VI = true;
  bool BE = true;
	bool BK = true;
  std::string phy = "ac";
  std::string mcs="VhtMcs9";

  RngSeedManager::SetSeed(seed);
	Packet::EnablePrinting ();
	Time simulationTime = Seconds (simTime);

	//Creating the Building
	//createBuilding();
    
  
	// Command line parameters 
  CommandLine cmd;
	cmd.AddValue ("simulationTime", "Simulation time [s]", simTime);
	cmd.AddValue ("distance", "Distance in meters between the station and the access point", distance);
	cmd.AddValue ("APs", "Number of Access Points", APs);
	cmd.AddValue ("stations", "Number of stations in each grid", nStations);
	cmd.AddValue ("rts", "Enable RTS/CTS", enableRtsCts);
	cmd.AddValue ("pcap", "Enable PCAP generation", pcap);
	cmd.AddValue ("packetSize", "Packet size [s]", packetSize);
	cmd.AddValue ("A_VO",       "run A_VO traffic?",                             A_VO);
  cmd.AddValue ("VO",         "run VO traffic?",                               VO);
  cmd.AddValue ("VI",         "run VI traffic?",                               VI);
  cmd.AddValue ("A_VI",       "run A_VI traffic?",                             A_VI);
  cmd.AddValue ("BE",         "run BE traffic?",                               BE);
  cmd.AddValue ("BK",         "run BK traffic?",                               BK);
  cmd.AddValue ("Mbps",       "traffic generated per queue [Mbps]", Mbps);
  cmd.AddValue ("seed",       "Seed", seed);
	cmd.AddValue ("OneDest",       "One destination traffic", oneDest);
	cmd.Parse (argc,argv);

	

    // Nodes
	NodeContainer wifiApNodes;
  NodeContainer wifiStaNodes;

	//Devices
  NetDeviceContainer apDevices;
	NetDeviceContainer staDevices;

	
	//for (int i = 0; i < APs; i++)
	//{
	wifiStaNodes.Create(nStations);
		
		//STA is located at (dista, 0, 0)
	mobilityAlloc(wifiStaNodes, distance);
	//BuildingsHelper::Install (wifiStaNodes);
	//}
	
    wifiApNodes.Create(APs);
    //Place AP and Stations created
	//AP is located at (0,0,0)
  mobilityAllocA(wifiApNodes, 0.0);
//BuildingsHelper::MakeMobilityModelConsistent ();
    // Configure Propagation Model
	
	//CHANNEL
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
	wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  //Set channel width
	Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (80));

	// MAC and PHY config
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
	
	
	WifiMacHelper wifiMac;
	WifiHelper wifi_helper;
	
	wifi_helper.SetStandard(WIFI_PHY_STANDARD_80211ac);
  //wifi_helper.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate54Mbps"), "ControlMode", StringValue ("OfdmRate6Mbps"), "MaxSlrc", UintegerValue (10));
  wifi_helper.SetRemoteStationManager ("ns3::MinstrelHtWifiManager", 
							      "RtsCtsThreshold",        UintegerValue (enableRtsCts ? 0 : 2500),
							      "FragmentationThreshold", UintegerValue (2500));
	//PHY
  wifiPhy.SetChannel (wifiChannel.Create ());
	wifiPhy.Set ("GuardInterval", TimeValue(NanoSeconds (800))); // LONG GI set
  wifiPhy.Set ("TxPowerStart", DoubleValue (20.0));
	wifiPhy.Set ("TxPowerEnd", DoubleValue (20.0));
	wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
	wifiPhy.Set ("TxGain", DoubleValue (0));
	wifiPhy.Set ("RxGain", DoubleValue (0));
	wifiPhy.Set ("RxNoiseFigure", DoubleValue (7));	
	wifiPhy.Set ("GuardInterval", TimeValue(NanoSeconds (800))); // LONG GI set
  wifiPhy.SetErrorRateModel ("ns3::YansErrorRateModel");
	wifiPhy.Set ("ShortGuardEnabled", BooleanValue (false));

    // MAC
	// First we set up MAC for AP(s)
    //Ssid ssid = Ssid("WiFi-VoIP");

    //for(int indexAP = 0; indexAP<APs; ++indexAP){
  Ssid ssid_ap = Ssid("WiFi-VoIP-"+std::to_string(APs));
		//Set up MAC for AP
  wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue (ssid_ap));
        //Here we install the phy and mac configured above into AP device
	NetDeviceContainer apdevice = wifi_helper.Install(wifiPhy, wifiMac, wifiApNodes);
        //AP added to NetDeviceContainer 
	apDevices.Add(apdevice);
    //}

	//PHY configuration for Stations
	wifiPhy.Set ("TxPowerStart", DoubleValue (15.0));
	wifiPhy.Set ("TxPowerEnd", DoubleValue (15.0));
	wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
	wifiPhy.Set ("TxGain", DoubleValue (-2)); // for STA -2 dBi

	//for each Station Created within Node Container install PHY and MAC
    //for(int index_ap = 0; index_ap < APs; ++index_ap){
        //Ssid ssid_sta = Ssid("WiFi-VoIP-"+std::to_string(APs));
        //Set UP MAC for Station
	wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue (ssid_ap));
        //Install above configured PHY and MAC
	NetDeviceContainer stadevice = wifi_helper.Install(wifiPhy, wifiMac, wifiStaNodes);
		//Add the configuration for each device within 
	staDevices.Add(stadevice);
    //}


    // Internet Stack Config
  InternetStackHelper stack;
	stack.Install(wifiApNodes);

    //for (int i = 0; i < APs; ++i)
    //{
	stack.Install(wifiStaNodes);
    //}

	// Configure addresses for nodes

	Ipv4AddressHelper address;
	Ipv4InterfaceContainer staIf;
	Ipv4InterfaceContainer apIf;
    
	//for(int i = 0; i < APs; ++i)
	//{
	std::string addrString;
	addrString =  "10.1." + std::to_string(APs) + ".0";
	const char *cstr = addrString.c_str(); //convert to constant char
	address.SetBase (Ipv4Address(cstr), "255.255.255.0");
	apIf = address.Assign (apDevices);
	//}
	staIf = address.Assign (staDevices);
	
	//Populate ARP Cache
    

	// QoS configurations
	//MinCw:
  	Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_EdcaTxopN/MinCw", UintegerValue (15) ); 

	//TXOP limit:
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_EdcaTxopN/TxopLimit", TimeValue (MicroSeconds (1504) ) ); 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_EdcaTxopN/TxopLimit", TimeValue (MicroSeconds (3008) ) ); 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_EdcaTxopN/TxopLimit", TimeValue (MicroSeconds    (0) ) ); 
	
	//EDCA max delay (NEW):
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_EdcaTxopN/MaxDelay", TimeValue (MicroSeconds  (10240) ) ); //setting VO packet lifetime = 10*TU (TU=1024 us)
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_EdcaTxopN/MaxDelay", TimeValue (MicroSeconds (102400) ) ); //setting VI packet lifetime = 100*TU (TU=1024 us)
	Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_EdcaTxopN/MaxDelay", TimeValue (MicroSeconds (512000) ) ); //setting BE packet lifetime = 500*TU (TU=1024 us)
	
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_EdcaTxopN/HiTidQueue/MaxPackets",  UintegerValue (10000)); //setting A_VO queue size 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_EdcaTxopN/LowTidQueue/MaxPackets", UintegerValue (10000)); //setting VO queue size 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_EdcaTxopN/HiTidQueue/MaxPackets",  UintegerValue (10000)); //setting VI queue size 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_EdcaTxopN/LowTidQueue/MaxPackets", UintegerValue (10000)); //setting A_VI queue size 
	Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_EdcaTxopN/Queue/MaxPackets",       UintegerValue (10000)); //setting BE queue size 
	
	
	/* Setting up applications */
	
	//Configure CBR traffic sources
	//DataRate dataRate = DataRate (1000000 * Mbps);
	uint32_t destinationSTANumber = nStations - 1;
	
	Ipv4Address destinationAddress = staIf.GetAddress (destinationSTANumber);
	Ptr<Node> destNode = wifiStaNodes.Get(destinationSTANumber);

   
     if (oneDest)
    {
      if (A_VO) 
        {
          PacketSinkHelper sink_A_VO ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1007));
          sink_A_VO.Install (destNode);
        }
      if (VO) 
        {
          PacketSinkHelper sink_VO ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1006));
          sink_VO.Install (destNode);
        }
      if (VI) 
        {
          PacketSinkHelper sink_VI ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1005));
          sink_VI.Install (destNode);
        }
      if (A_VI) 
        {
          PacketSinkHelper sink_A_VI ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1004));
          sink_A_VI.Install (destNode);
        }
      if (BE) 
        {
          PacketSinkHelper sink_BE ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1000));
          sink_BE.Install (destNode);
        }
      if (BK) 
        {
          PacketSinkHelper sink_BK ("ns3::UdpSocketFactory", InetSocketAddress (destinationAddress, 1001));
          sink_BK.Install (destNode);
        }
    }


	int appList [] = {0, 1, 2, 3, 4, 5, 3, 3, 3, 2 };

	//uint64_t kbps = 64;
	//DataRate videoRate = DataRate (100 * kbps);
	//DataRate audioRate = DataRate (100 * kbps);
   
	for (uint32_t iSTA = 0; iSTA < nStations; iSTA++)
	{
		Ptr<Node> node = wifiStaNodes.Get(iSTA);

      if (!oneDest) //overwrite for different traffic destinations
        {
          destinationSTANumber = (iSTA+1 == nStations) ? (0) : (iSTA+1); 
          destinationAddress = staIf.GetAddress(destinationSTANumber);
          destNode = wifiStaNodes.Get(destinationSTANumber);
        }

      if (appList[iSTA] == 0)
        {
          OnOffHelper A_VO_ = trafficHelper(InetSocketAddress (destinationAddress, 1007), DataRate(1000 * 64), packetSize, 7, appsStart, simulationTime);
          A_VO_.Install(node);
        }
      if (appList[iSTA] == 1)
        {
          OnOffHelper A_VI_ = trafficHelper(InetSocketAddress (destinationAddress, 1004), DataRate(1000000 * 2), packetSize, 4, appsStart, simulationTime);
          A_VI_.Install(node);
        }
      if (appList[iSTA] == 2)
        {
          OnOffHelper VO_ = trafficHelper(InetSocketAddress (destinationAddress, 1006), DataRate(1000 * 64), packetSize, 6, appsStart, simulationTime);
          VO_.Install(node);
        }
      if (appList[iSTA] == 3)
        {
          OnOffHelper VI_ = trafficHelper(InetSocketAddress (destinationAddress, 1005), DataRate(1000000 * 3), packetSize, 5, appsStart, simulationTime);
          VI_.Install(node);
        }
      if (appList[iSTA] == 4)
        {
          OnOffHelper BE_ = trafficHelper(InetSocketAddress (destinationAddress, 1000), DataRate(1000 * 50), packetSize, 0, appsStart, simulationTime);
          BE_.Install(node); 
        }
      if (appList[iSTA] == 5)
        {
          OnOffHelper BK_ = trafficHelper(InetSocketAddress (destinationAddress, 1001), DataRate(1000 * 50), packetSize, 1, appsStart, simulationTime);
          BK_.Install(node);
        }
   }


	 

	/* 
	for (uint32_t iSTA = 0; iSTA < nStations; iSTA++)
	{
		Ptr<Node> node = wifiStaNodes.Get(iSTA);

      if (!oneDest) //overwrite for different traffic destinations
        {
          destinationSTANumber = (iSTA+1 == nStations) ? (0) : (iSTA+1); 
          destinationAddress = staIf.GetAddress(destinationSTANumber);
          destNode = wifiStaNodes.Get(destinationSTANumber);
        }

      if (A_VO) 
        {
          OnOffHelper A_VO_ = trafficHelper(InetSocketAddress (destinationAddress, 1007), dataRate, packetSize, 7, appsStart, simulationTime);
          A_VO_.Install(node);
        }
      if (A_VI) 
        {
          OnOffHelper A_VI_ = trafficHelper(InetSocketAddress (destinationAddress, 1004), dataRate, packetSize, 4, appsStart, simulationTime);
          A_VI_.Install(node);
        }
      if (VO) 
        {
          OnOffHelper VO_ = trafficHelper(InetSocketAddress (destinationAddress, 1006), dataRate, packetSize, 6, appsStart, simulationTime);
          VO_.Install(node);
        }
      if (VI) 
        {
          OnOffHelper VI_ = trafficHelper(InetSocketAddress (destinationAddress, 1005), dataRate, packetSize, 5, appsStart, simulationTime);
          VI_.Install(node);
        }
      if (BE) 
        {
          OnOffHelper BE_ = trafficHelper(InetSocketAddress (destinationAddress, 1000), dataRate, packetSize, 0, appsStart, simulationTime);
          BE_.Install(node); 
        }
      if (BK) 
        {
          OnOffHelper BK_ = trafficHelper(InetSocketAddress (destinationAddress, 1001), dataRate, packetSize, 1, appsStart, simulationTime);
          BK_.Install(node);
        }

	}*/
	
	SimulationHelper::PopulateArpCache();
	Simulator::Stop (simulationTime);

	if (pcap)
		wifiPhy.EnablePcap("VoiP-WiFi", nStations, 0);


	FlowMonitorHelper flowmon_helper;
	Ptr<FlowMonitor> monitor = flowmon_helper.InstallAll ();
	monitor->SetAttribute ("StartTime", TimeValue (Seconds (calcStart) ) ); //Time from which flowmonitor statistics are gathered.
	monitor->SetAttribute ("DelayBinWidth", DoubleValue (0.001));
	monitor->SetAttribute ("JitterBinWidth", DoubleValue (0.001));
	monitor->SetAttribute ("PacketSizeBinWidth", DoubleValue (20));
	
	
	// Run Simulation
	Simulator::Run ();
  Simulator::Destroy ();




	/* ===== printing results ===== */
	monitor->CheckForLostPackets ();

	//monitor->SerializeToXmlFile ("out.xml", true, true); // sniffing to XML file
	
	std::string proto;
	//initialize variables for overall results calculation
	uint64_t txBytes = 0, rxBytes = 0, txPackets = 0, rxPackets = 0, lostPackets = 0;
	double throughput;
	Time delaySum = Seconds (0), jitterSum = Seconds (0);

	Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon_helper.GetClassifier ());
	//iterate over traffic flows
	std::map< FlowId, FlowMonitor::FlowStats > stats = monitor->GetFlowStats ();
	for (std::map<FlowId, FlowMonitor::FlowStats>::iterator flow = stats.begin (); flow != stats.end (); flow++)
		{
		Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (flow->first);

		//print results for given traffic flow
		switch (t.protocol)
			{
			case (6):
				proto = "TCP";
				break;
			case (17):
				proto = "UDP";
				break;
			default:
				exit (1);
			}
		std::cout << "FlowID: " << flow->first << "(" << proto << " "
					<< t.sourceAddress << "/" << t.sourcePort << " --> "
					<< t.destinationAddress << "/" << t.destinationPort << ")" <<
		std::endl;

		std::cout << "  Tx bytes:\t"     << flow->second.txBytes << std::endl;
		std::cout << "  Rx bytes:\t"     << flow->second.rxBytes << std::endl;
		std::cout << "  Tx packets:\t"   << flow->second.txPackets << std::endl;
		std::cout << "  Rx packets:\t"   << flow->second.rxPackets << std::endl;
		std::cout << "  Lost packets:\t" << flow->second.lostPackets << std::endl;
		if (flow->second.rxPackets > 0)
			{
			//std::cout << "  Throughput:\t"   << flow->second.rxBytes * 8.0 / (flow->second.timeLastRxPacket.GetSeconds ()-flow->second.timeFirstTxPacket.GetSeconds ()) / 1000000  << " Mb/s" << std::endl;
			std::cout << "  Throughput:\t"   << flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds ()  << " Mb/s" << std::endl;
			std::cout << "  Mean delay:\t"   << (double)(flow->second.delaySum / (flow->second.rxPackets)).GetMicroSeconds () / 1000 << " ms" << std::endl;    
			if (flow->second.rxPackets > 1)
				std::cout << "  Mean jitter:\t"  << (double)(flow->second.jitterSum / (flow->second.rxPackets - 1)).GetMicroSeconds () / 1000 << " ms" << std::endl;   
			else
				std::cout << "  Mean jitter:\t---"   << std::endl;
			}
		else
			{
			std::cout << "  Throughput:\t0 Mb/s" << std::endl;
			std::cout << "  Mean delay:\t---"    << std::endl;    
			std::cout << "  Mean jitter:\t---"   << std::endl;
			}

		//increase variables for overall results calculation
		txBytes     += flow->second.txBytes;
		rxBytes     += flow->second.rxBytes;
		txPackets   += flow->second.txPackets;
		rxPackets   += flow->second.rxPackets;
		lostPackets += flow->second.lostPackets;
		//throughput  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (flow->second.timeLastRxPacket.GetSeconds ()-flow->second.timeFirstTxPacket.GetSeconds ()) / 1000000 : 0);
		throughput  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds () : 0);
		delaySum    += flow->second.delaySum;
		jitterSum   += flow->second.jitterSum;
		}


	//print overall results
	std::cout << "=======================Total: =====================================" << std::endl;

	std::cout << "  Tx bytes:\t"     << txBytes     << std::endl;
	std::cout << "  Rx bytes:\t"     << rxBytes     << std::endl;
	std::cout << "  Tx packets:\t"   << txPackets   << std::endl;
	std::cout << "  Rx packets:\t"   << rxPackets   << std::endl;
	std::cout << "  Lost packets:\t" << lostPackets << std::endl;
	std::cout << "  Throughput:\t"   << throughput  << " Mb/s" << std::endl;
	if (rxPackets > 0)
		{
		std::cout << "  Mean delay:\t"   << (double)(delaySum / (rxPackets)).GetMicroSeconds () / 1000 << " ms" << std::endl;    
		if (rxPackets > 1)  
			std::cout << "  Mean jitter:\t"  << (double)(jitterSum / (rxPackets - 1)).GetMicroSeconds () / 1000  << " ms" << std::endl;   
		else
			std::cout << "  Mean jitter:\t---"   << std::endl;
		}
	else
		{
		std::cout << "  Mean delay:\t---"    << std::endl;    
		std::cout << "  Mean jitter:\t---"   << std::endl;
		}


    return 0;


}


void mobilityAlloc(NodeContainer &nodes, double dist){
	  
	double x_min = 0.0;
	double x_max = 1.0;
	double y_min = 0.0;
	double y_max = 1.0;
	double z_min = 0.0;
	double z_max = 1.0;
	Ptr<Building> b = CreateObject <Building> ();
	b->SetBoundaries (Box (x_min, x_max, y_min, y_max, z_min, z_max));
	b->SetBuildingType (Building::Residential);
	b->SetExtWallsType (Building::ConcreteWithWindows);
	b->SetNFloors (1);
	b->SetNRoomsX (1);
	b->SetNRoomsY (1);

	MobilityHelper mobility;
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.SetPositionAllocator("ns3::RandomRoomPositionAllocator");
	mobility.Install (nodes);
	BuildingsHelper::Install (nodes);
	BuildingsHelper::MakeMobilityModelConsistent();

}

void mobilityAllocA(NodeContainer &nodes, double dist){
    MobilityHelper mobility;
	uint32_t nNodes = nodes.GetN();
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
    
	positionAlloc->Add (Vector (0.0, 0.0, 0.0));
 	
	 for (uint32_t i = 0; i < nNodes; i++){
	 	positionAlloc->Add (Vector (dist, 0.0, 0.0));
	 }
	  
	/*double x_min = 5.0;
	double x_max = 10.0;
	double y_min = 5.0;
	double y_max = 15.0;
	double z_min = 5.0;
	double z_max = 15.0;
	Ptr<Building> b = CreateObject <Building> ();
	b->SetBoundaries (Box (x_min, x_max, y_min, y_max, z_min, z_max));
	b->SetBuildingType (Building::Residential);
	b->SetExtWallsType (Building::ConcreteWithWindows);
	b->SetNFloors (3);
	b->SetNRoomsX (5);
	b->SetNRoomsY (5);

	MobilityHelper mobility;*/
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.SetPositionAllocator("ns3::RandomRoomPositionAllocator");
	mobility.Install (nodes);
	BuildingsHelper::Install (nodes);

	
    //mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    //mobility.Install(nodes);

}


//fullfil the ARP cache prior to simulation run
void SimulationHelper::PopulateArpCache () 
{
  Ptr<ArpCache> arp = CreateObject<ArpCache> ();
  arp->SetAliveTimeout (Seconds (3600 * 24 * 365) );
	
  for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i) 
    {	
      Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
      NS_ASSERT (ip != 0);
      ObjectVectorValue interfaces;
      ip->GetAttribute ("InterfaceList", interfaces);

      for (ObjectVectorValue::Iterator j = interfaces.Begin (); j != interfaces.End (); j++) 
        {		
          Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
          NS_ASSERT (ipIface != 0);
          Ptr<NetDevice> device = ipIface->GetDevice ();
          NS_ASSERT (device != 0);
          Mac48Address addr = Mac48Address::ConvertFrom (device->GetAddress () );
      
          for (uint32_t k = 0; k < ipIface->GetNAddresses (); k++) 
            {			
              Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal();		
              if (ipAddr == Ipv4Address::GetLoopback ()) 
                continue;

              ArpCache::Entry *entry = arp->Add (ipAddr);
              Ipv4Header ipv4Hdr;
              ipv4Hdr.SetDestination (ipAddr);
              Ptr<Packet> p = Create<Packet> (100);  
              entry->MarkWaitReply (ArpCache::Ipv4PayloadHeaderPair (p, ipv4Hdr) );
              entry->MarkAlive (addr);
            }
        }
    }

    for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i) 
      {
        Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
		NS_ASSERT (ip != 0);
		ObjectVectorValue interfaces;
		ip->GetAttribute ("InterfaceList", interfaces);

        for (ObjectVectorValue::Iterator j = interfaces.Begin (); j != interfaces.End (); j ++)
          {
            Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
            ipIface->SetAttribute ("ArpCache", PointerValue (arp) );
          }
      }
}


void createBuilding(){

	double xMax = 50.0;
	double yMax = 50.0;
	double zMax = 50.0;

	Ptr<Building> scenarioBuilding = CreateObject <Building>();

	scenarioBuilding->SetBoundaries (Box(0.0, xMax, 0.0, yMax, 0.0, zMax));
	scenarioBuilding->SetBuildingType(Building::Office);
	scenarioBuilding->SetExtWallsType (Building::ConcreteWithWindows);
	scenarioBuilding->SetNRoomsX(5);
	scenarioBuilding->SetNRoomsY(5);
	
}


OnOffHelper trafficHelper (InetSocketAddress socketAddress, DataRate dataRate, int packetSize, uint8_t tid, Time appsStart, Time appsStop){
	
	socketAddress.SetTos (tid << 5);
	OnOffHelper cbr ("ns3::UdpSocketFactory", socketAddress);
	cbr.SetAttribute("StartTime", TimeValue (appsStart) );
	cbr.SetAttribute ("StopTime", TimeValue (appsStop) );
	cbr.SetAttribute ("DataRate",   DataRateValue (dataRate) );
  	cbr.SetAttribute ("PacketSize", UintegerValue (packetSize) );

	return cbr;
}


void installTrafficGenerator(Ptr<ns3::Node> fromNode, Ptr<ns3::Node> toNode, int port, std::string offeredLoad, int packetSize, int simulationTime, int warmupTime ) {

	Ptr<Ipv4> ipv4 = toNode->GetObject<Ipv4> (); // Get Ipv4 instance of the node
	Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal (); // Get Ipv4InterfaceAddress of xth interface.

	ApplicationContainer sourceApplications, sinkApplications;

	uint8_t tosValue = 0x70; //AC_BE
	
	//Add random fuzz to app start time
	double min = 0.0;
	double max = 1.0;
	Ptr<UniformRandomVariable> fuzz = CreateObject<UniformRandomVariable> ();
	fuzz->SetAttribute ("Min", DoubleValue (min));
	fuzz->SetAttribute ("Max", DoubleValue (max));		

	InetSocketAddress sinkSocket (addr, port);
	sinkSocket.SetTos (tosValue);
	OnOffHelper onOffHelper ("ns3::UdpSocketFactory", sinkSocket);
	onOffHelper.SetConstantRate (DataRate (offeredLoad + "Mbps"), packetSize);
	sourceApplications.Add (onOffHelper.Install (fromNode)); //fromNode
	PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", sinkSocket);
	sinkApplications.Add (packetSinkHelper.Install (toNode)); //toNode

	sinkApplications.Start (Seconds (warmupTime));
	sinkApplications.Stop (Seconds (simulationTime));
	sourceApplications.Start (Seconds (warmupTime+fuzz->GetValue ()));
	sourceApplications.Stop (Seconds (simulationTime));




}



OnOffHelper voiceTraffic(InetSocketAddress socketAddress, DataRate rate, uint32_t pSize, Time appsStart, Time simTime){
	OnOffHelper voice ("ns3::UdpSocketFactory", socketAddress);
	voice.SetConstantRate (rate, pSize);
	voice.SetAttribute ("OnTime", StringValue("ns3::ExponentialRandomVariable[Mean=1]") );
	voice.SetAttribute ("OffTime", StringValue("ns3::ExponentialRandomVariable[Mean=1.3]") );
	voice.SetAttribute ("StartTime", TimeValue (appsStart) );
	voice.SetAttribute ("StopTime", TimeValue (simTime) );

	return voice;
}

OnOffHelper videoTraffic(InetSocketAddress socketAddress, DataRate rate, uint32_t pSize, Time appsStart, Time simTime){
	OnOffHelper video ("ns3::UdpSocketFactory", socketAddress);
	video.SetConstantRate (rate, pSize);
	video.SetAttribute ("OnTime", StringValue("ns3::ExponentialRandomVariable[Mean=0.001]") );
	video.SetAttribute ("OffTime", StringValue("ns3::ExponentialRandomVariable[Mean=0.976]") );
	video.SetAttribute ("StartTime", TimeValue (appsStart) );
	video.SetAttribute ("StopTime", TimeValue (simTime) );

	return video;
}

